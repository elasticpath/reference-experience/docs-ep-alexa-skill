---
id: index
title: Reference Alexa Skill Quick Start Guide
sidebar_label: Overview
---

This document provides guidelines to setup and configure the Reference Alexa Skill. However, this document is not a primer for JavaScript and is intended for professionals who are familiar with the following technologies:

* [Nodejs](https://nodejs.org/en/)
* [AWS (Amazon Web Services) Lambda Functions](https://aws.amazon.com/lambda/)
* [Alexa Voice Services](https://developer.amazon.com/alexa-voice-service)

## Overview

The Reference Alexa Skill is a flexible IoT skill, which communicates with Elastic Path’s RESTful e-commerce API, Cortex API. Through the Cortex API, the skill uses the e-commerce capabilities provided by Elastic Path Commerce and interacts with data in a RESTful manner.

![Architecture Diagram](assets/architecture.png)

## Project Structure

You can find the lambda function in the `lambda/main` folder.

You can find the Interaction Model for the skill in the `models/en-US.json` file.  For more information about the Interaction Model, see [Alexa Developer Documentation](https://developer.amazon.com/docs/custom-skills/create-the-interaction-model-for-your-skill.html).

## Requirements

To install and customize Reference Alexa Skill, you must have the following software:

* [Git](https://git-scm.com/downloads)
* [Node.js](https://nodejs.org/en/download/)
* A publicly available [Account Linking Server](./login-server.html)
* A valid [Amazon Web Services (AWS) Account](https://console.aws.amazon.com)
* A valid [Amazon Developer Account](https://developer.amazon.com)
* A valid Elastic Path development environment. For more information, see [The Starting Construction Guide](https://developers.elasticpath.com/commerce/construction-home)
