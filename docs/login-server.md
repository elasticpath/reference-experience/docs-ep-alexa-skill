---
id: login-server
title: Setting up the Login server
sidebar_label: Login server
---

This topic provides guidelines to setup and configure the Reference Alexa Skill’s Login Server, to serve as the authentication bridge between the Reference Alexa Skill and Amazon Alexa services. This enables shoppers to interact with the Alexa skill using their accounts through Cortex.

## Setting up Account Linking

1. Set up the [Reference Account Linking server](https://github.com/elasticpath/account-linking)

## Configuring Account Linking in Alexa

1. Log into the Alexa Developer account at `developer.amazon.com`, and go to  `Alexa`>`Your Alexa Consoles`> `Skills`>`<Your Skill Name>`
2. On the left sidebar, scroll down and select `Account Linking`
3. In the `Do you allow users to create an account or link to an existing account with you?` field, set it to `On`
4. In the `Security Provider Information` field, select `Implicit Grant`
5. Fill in the fields under Security Provider Information. For more information, see the preceding chart.

    |Field name | field|
    |----------|-------|
    |Authorization URI| Set to the endpoint of the Account Linking server you set up in step one (**Note:** An `https://` endpoint is required)|
    |Client ID | An identifier for your skill |
    |Scope | An optional list of scopes that indicating the access the Alexa user needs. |
    |Domain List | Set to the Domain of your Account Linking endpoint |

6. Click **Save**

## Test Account Linking

1. Go to the **Test** tab and invoke the skill.
    **Note**: Alexa responds with asking the user to link account. For more information about account linking with Alexa Skills, see the [How users connect](https://developer.amazon.com/docs/account-linking/account-linking-for-custom-skills.html#how-users-connect) section
