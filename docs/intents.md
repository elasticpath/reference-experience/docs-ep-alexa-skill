---
id: intents
title: Intents Reference
sidebar_label: Intents
---

The following table describes the intents the skill uses, the actions they represent, and some sample phrases to use.

:::note
If you find an error, inform the developer which intent triggered the error.
:::

## Elastic Path Intents

For more information about sample phrases, see the [Interaction Model](https://github.com/elasticpath/alexa-skill/blob/master/models/en-US.json).

| Action                        | Sample Utterance                               | Intent Name                                             |
|-------------------------------|------------------------------------------------|---------------------------------------------------------|
| About Store                   | `"What do you sell?"`                          | `DescribeStoreIntent`                                   |
| Search                        | `"Search for {item}"`                          | `KeywordSearchIntent`                                   |
|                               | `"Find {item}"`                                |                                                         |
|                               | `"Do you have any {item}"`                     |                                                         |
|                               | `"look for {item}"`                            |                                                         |
|                               | `"search the store for {item}"`                |                                                         |
|                               | `"what {item} do you have"`                    |                                                         |
| Next                          | `"Next item"`                                  | `NextItemIntent`                                        |
|                               | `"Go forward"`                                 |                                                         |
|                               | `"Show me something else"`                     |                                                         |
|                               | `"What else do you have"`                      |                                                         |
|                               | `"Show me the next item"`                      |                                                         |
|                               | `"What's next"`                                |                                                         |
|                               | `"Next"`                                       |                                                         |
| Previous                      | `"Previous item"`                              | `PreviousItemIntent`                                    |
|                               | `"Show me the last item"`                      |                                                         |
|                               | `"What was the last item"`                     |                                                         |
|                               | `"Previous"`                                   |                                                         |
|                               | `"Go back"`                                    |                                                         |
|                               | `"Last"`                                       |                                                         |
| Describe Current Product      | `"Tell me more about that"`                    | `DescribeProductIntent` / `DescribeListedProductIntent` |
|                               | `"show me"`                                    |                                                         |
|                               | `"what is that"`                               |                                                         |
|                               | `"what is this"`                               |                                                         |
|                               | `"what's that"`                                |                                                         |
|                               | `"describe it"`                                |                                                         |
|                               | `"describe this item"`                         |                                                         |
|                               | `"tell me more about the item"`                |                                                         |
|                               | `"i want to know more about it"`               |                                                         |
|                               | `"i want to hear more"`                        |                                                         |
|                               | `"tell me about it"`                           |                                                         |
|                               | `"tell me about them"`                         |                                                         |
|                               | `"give me more info about the product"`        |                                                         |
| Describe Product Price        | `"what's it cost"`                             | `DescribePriceIntent`                                   |
|                               | `"what's the price"`                           |                                                         |
| Describe Product Availability | `"is it in stock"`                             | `DescribeInventoryIntent`                               |
|                               | `"is it available"`                            |                                                         |
| Add to Cart                   | `"Add that to my cart"`                        | `AddToCartIntent`                                       |
|                               | `"add to cart"`                                |                                                         |
|                               | `"add {ItemQuantity} of the item to my cart"`  |                                                         |
|                               | `"add {ItemQuantity} to my cart"`              |                                                         |
|                               | `"put {ItemQuantity} in my cart"`              |                                                         |
|                               | `"add {ItemQuantity} to cart"`                 |                                                         |
|                               | `"add {ItemQuantity} of those to my cart"`     |                                                         |
|                               | `"add {ItemQuantity} of that to my cart"`      |                                                         |
|                               | `"add {ItemQuantity} of that item to my cart"` |                                                         |
| Add to Wishlist               | `"Add that to my wishlist"`                    | `AddToWishlistIntent`                                   |
|                               | `"add the item to my wish list"`               |                                                         |
|                               | `"add it to my wish list"`                     |                                                         |
|                               | `"put it in my wish list"`                     |                                                         |
|                               | `"add it to wish list"`                        |                                                         |
|                               | `"add it to my wishlist"`                      |                                                         |
|                               | `"put it in my wishlist"`                      |                                                         |
| Explore Cart                  | `"What's in my cart?"`                         | `GetCartIntent`                                         |
|                               | `"See shopping cart"`                          |                                                         |
|                               | `"Check cart"`                                 |                                                         |
|                               | `"Cart"`                                       |                                                         |
|                               | `"Go to cart."`                                |                                                         |
|                               | `"Go to my cart."`                             |                                                         |
|                               | `"Go to the cart."`                            |                                                         |
| Explore Wishlist              | `"What's in my wishlist?"`                     | `GetWishlistIntent`                                     |
|                               | `"What's on my wish list"`                     |                                                         |
| Move to Wishlist              | `"move item {ItemNumber} to my wish list"`     | `MoveToWishlistIntent`                                  |
|                               | `"put item {ItemNumber} on my wish list"`      |                                                         |
| Move to Cart                  | `"Move item {ItemNumber} to my cart"`          | `MoveToCartIntent`                                      |
|                               | `"put item {ItemNumber} in my cart"`           |                                                         |
| Remove from Cart              | `"Remove item number three from my cart"`      | `RemoveFromCartIntent`                                  |
|                               | `"remove"`                                     |                                                         |
|                               | `"remove that"`                                |                                                         |
|                               | `"remove from cart"`                           |                                                         |
|                               | `"remove from my cart"`                        |                                                         |
|                               | `"remove this item from my cart"`              |                                                         |
| Remove from Wishlist          | `"take item {ItemNumber} off my wish list"`    | `RemoveFromWishlistIntent`                              |
|                               | `"take item {ItemNumber} off my wish list"`    |                                                         |
|                               | `"delete item {ItemNumber} from my wish list"` |                                                         |
| Checkout                      | `"I'd like to check out"`                      | `CheckOutIntent`                                        |
|                               | `"checkout my cart"`                           |                                                         |
|                               | `"checkout"`                                   |                                                         |

## Default Alexa Intents

The Alexa Reference Skill also incorporates several default Alexa intents.

| Action                    | Sample Utterance                          | Intent Name                                           |
| ------------------------- | ----------------------------------------- | ----------------------------------------------------- |
| Yes                       | `"Yes"`                                     | `AMAZON.YesIntent`                                    |
| No                        | `"No"`                                      | `AMAZON.NoIntent`                                     |
| Help                      | `"Help / What are my options?"`             | `AMAZON.HelpIntent`                                   |
| Exit                      | `"Alexa, stop / exit"`                      | `AMAZON.StopIntent`                                   |
| Cancel                    | `"Cancel"`                                 | `AMAZON.CancelIntent`                                 |
