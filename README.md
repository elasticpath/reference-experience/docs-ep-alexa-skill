# Documentation

This repository contains documentation of Alexa-Skill. Documentation is built and published from this repository and is available at [Elastic Path Documentation site][doc-site].

## Table of content

1. [Pre-requisites](#pre-requisites)
1. [Quick Start](#quick-start)
1. [Contributing](#contributing)
1. [Usage](#usage)
    1. [Overview of the repository structure](#overview-of-the-repository-structure)
    1. [Build & Test using Gradle](#using-gradle)

## Pre-requisites

### Git

Git is required if you would like to work with this repository on your local machine or would like to contribute changes to this repository.

### Java

The only required tool to work with this repository is *Java* because *Gradle* is used as the build tool. And *Gradle* itself will be downloaded to your home directory and configured automatically along with any other tools necessary (i.e. NodeJs, Yarn), if you don't already have the required tools.

### Editor

You'll also need a text editor (i.e Visual Studio Code, Atom, IntelliJ IDEA etc.). Once the repository has been cloned to your local directory, open the directory in your favourite text editor.

### EditorConfig

This repository is also configured with "EditorConfig" to make sure everyone's favourite editor is configured the same way, such as: using spaces for indentation, new line at the end of file, etc. Some editors have built-in support for EditorConfig. Please [download and install EditorConfig plugin for your Editor](https://editorconfig.org/#download), if you're using one of the editors that does not have this support out of the box.

## Quick Start

It's very easy to get started with this repository. Besides Git, all you need is Java and you're set. To get started:

1. Open a terminal or command prompt from your machine and go to a directory or location where you'd like to clone this repository
1. Clone this repository: `git clone <ssh-or-https-url-of-this-repo>`
1. Run `./gradlew startDevServer`
    - If you don't already have the required Gradle version, it will be downloaded automatically
    - It will also download and setup NodeJS, Yarn, npm packages, etc. as needed by this repository

Your default browser should open automatically with the site running on your local machine. If not, you can access it at http://localhost:3000/alexa-skill/

Let's do a quick test

1. Click on the *Documentation* link from the navigation bar at the top
1. Edit some content of the file `docs/index.md` and save
1. Your changes should now appear on the browser

Great!! Now you know how to start contributing to the docs :). To stop your dev server, in your terminal/command prompt press *control + C* or *CTRL + C*.

## Contributing

In order to contribute to this repository, please follow these steps:

1. Ensure that you've clone the git repository to your local machine
1. Create a new branch from the latest `master` branch
2. Commit your changes
    - Follow the [instructions in usage section](#usage)
    - Be sure to validate your change by running the dev server on your local machine and also running `./gradlew test`. For more details, see [build & test](#build--test)
3. Push your changes upstream and submit a Pull Request for review
    - If other changes have been merged into `master` branch in the meantime, please rebase your branch on the latest of the `master` branch


## Usage

### Overview of the repository structure

The structure this repository should look like this:

```
.
├── README.md
├── docs
│   ├── assets
│   │   ├── ...
│   ├── deployment
│   │   ├── deploy.md
│   │   ├── index.md
│   ├── glossary.md
│   ├── index.md
│   ├── release-notes.md
├── build.gradle
├── gradle
├── gradlew 
├── gradlew.bat
├── settings.gradle
├── .editorconfig
└── website
    ├── build.gradle
    ├── package.json
    ├── sidebars.json
    ├── siteConfig.js
    ├── versioned_docs
    │   └── version-1.0.0
    ├── versioned_sidebars
    │   └── version-1.0.0-sidebars.json
    ├── versions.json
```
#### repository directory

This directory contains all necessary directories. It also holds the main Gradle script and configuration, which can be used for build and test either in your local environment or in CI environment.

#### `docs` directory

This directory contains all the documentation for the **Next** release. If there is no versioning involved, then it represents the **current/latest** release docs. In the example above:

- there's a directory named `assets`, which holds all images that may need to be refered to from a documentation
- documents are structured in a logical manner such as release notes, glossary, etc are at the root and all *deployment* related docs are in `deployment` directory

#### `website` directory

This directory holds all the configuration of *Docusaurus*, which is used as the tool for building and managing documentation. Some of the key files and directories are:

- `build.gradle`: Gradle build script
- `package.json`: All NPM packages used by this repository
- `sidebars.json`: Configuration file for sidebars of all documents for **Next** release if versioning is used; otherwise it's used for current release
- `siteConfig.json`: Main configuration file for Docusaurus
- `versioned_docs`: If versioning is used, all of the versions will be in their own directory with corresponding version number
- `versioned_sidebars`: This directory contains sidebars for all the releases in their own file with corresponding version number
- `versions.json`: A configuration file listing all the versions

### Build & Test

#### Using Gradle

From your terminal/command prompt, you will need to be in the repository directory to execute any commands.

- Go to the repository directory
- Run `./gradlew clean build` to build the site using a clean environment
    - Built artifats (static site html) will be available in `website/build` directory of this repository
- Run `./gradlew test` to execute all the tests setup for validating documentation guidelines and styles

Refer to the [list of commands](#list-of-all-relevant-commands) for details of various commands that can be executed.

#### Gradle commands 

##### Dev tasks:
- `startDevServer`: Starts the development server and launches a local version of the site in a browser. Use the browser to check the content and presentation before submitting new and changed files for review.
- `clean`: Deletes the `build` directory and any other temporary files and directories that are created during the build.
- `build`: Builds a static HTML site. Our CI pipeline deploys this artifacts into deployment environments.

##### Test tasks:
- `test`: Executes the configured tests/linters. Only changed files will be tested against a set of writing styles. If any files are deleted, all files in the corresponding version will be tested for broken relative links. Files that have been committed into git already, will not be validated.
- `test_allFiles`: Executes the configured tests/linters on all docs in the repository

Following tasks are used by the above two tasks but they can also be used independently.
- `markdownlint_changedFiles`: Runs [markdownlint-cli](https://github.com/igorshubovych/markdownlint-cli) tool on changed files only to validate that the files use a standardized Markdown format. For more information about the rules we apply, see [Markdown Style](https://elasticpath.atlassian.net/wiki/spaces/PD/pages/1736996/Markdown+Style) 
- `markdownlint_allFiles`: Performs same tasks as the previous task but it validates all docs in the repository
- `textlint_changedFiles`: Runs [textlint](https://textlint.github.io/) tool on changed files only to validate the content follows standards for writing clear content. For more information about the rules we apply, see [Writing Style](https://elasticpath.atlassian.net/wiki/spaces/PD/pages/1737050/Writing+Style) 
- `textlint_allFiles`: Performs same tasks as the previous task but it validates all docs in the repository.
- `validateExternalLinks_changedFiles`: Scans changed files for external links that are no longer valid/dead.
- `validateExternalLinks_allFiles`: Scans all files for external links that are no longer valid/dead. 
- `validateRelativeLinks_changedFiles`: Scans all files relevant to changed files for broken relative links.
- `validateRelativeLinks_allFiles`: Scans all files for broken relative links.

Following helpful tasks are also available to be executed by users if needed:
- `textlintWithAutofix_changedFiles`: This task will only apply to files that have been changed but not committed yet. Runs the same validation check as `textlint*` tasks mentioned earlier, but applies fixes automatically where possible. Be sure to review the lines that are reported as updated after executing this task. For example, it will apply following changes:
   - Replace single quote character with a proper apostrophe character.
   - Convert the first letter of a sentence to uppercase if it was in lowercase
- `textlintWithAutofix_allFiles`: Same as the previous task but it will apply on all documents.
- `textlintWriteGood`: Scans all files for good writing practices.
- `mdspell_changedFiles`: Checks changed documents for common spelling mistakes.
- `mdspell_allFiles`: Checks all documents for common spelling mistakes.

 

[doc-site]: https://documentation.elasticpath.com/alexa-skill
[docusaurus-site]: https://docusaurus.io
[markdownlint-cli]: https://github.com/igorshubovych/markdownlint-cli
[textlint]: https://textlint.github.io
