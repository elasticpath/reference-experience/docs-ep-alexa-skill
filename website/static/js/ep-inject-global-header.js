/**
 * The global header is built as a custom html element or webcomponent.
 * The custom element and necessary pollyfils must be added to the site
 * using the `scripts` object in the `siteConfig.js` configuration file.
 *
 * Assuming the above is done, the global header custom element needs to
 * be injected into the page as we don't have control over the "header"
 * of the site.
 */

document.addEventListener("DOMContentLoaded", function() {

    //create the dom element of the global header and configure it
    let epSiteHeader = document.createElement("ep-site-nav");
    epSiteHeader.setAttribute('siteName', 'Documentation');

    //Add the global header to the body of the site as the first element
    let globalHeaderContainer = document.createElement("div");
    globalHeaderContainer.className = "globalHeader";
    globalHeaderContainer.appendChild(epSiteHeader);
    document.body.prepend(globalHeaderContainer);

});
