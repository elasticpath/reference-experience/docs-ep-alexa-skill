module.exports = (siteUrl) => [
  {
    src: siteUrl + "js/ep-google-tag-manager.js",
    async: true
  },
  {
    src: 'https://buttons.github.io/buttons.js',
    async: true
  },
  {
    src: 'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js',
    async: true
  },
  {
    src: siteUrl + 'js/code-block-button.js',
    async: true
  },

  /**
   * With the 'defer' attribute specified, we need to order the scripts as:
   * 1. 'webcomponents-loader.js' which loads all the webcomponents
   * 2. Any webcomponent scripts, e.g. 'EpSiteNav.js'
   * 3. Any script that uses the webcomponents, e.g. 'ep-inject-global-header.js'
   */
  {
    src: 'https://documentation.elasticpath.com/_lib/ep-webcomponent/webcomponents-loader.js',
    defer: true
  },
  {
    src: 'https://documentation.elasticpath.com/_lib/ep-webcomponent/public/EpSiteNav.js',
    defer: true
  },
  {
    src: 'https://documentation.elasticpath.com/_lib/ep-webcomponent/public/EpSiteFooter.js',
    defer: true
  },
  {
    src: siteUrl + "js/ep-inject-global-header.js",
    defer: true
  }
]
