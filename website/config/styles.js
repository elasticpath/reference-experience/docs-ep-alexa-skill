module.exports = {
  /* Colors for website */
  colors: {
    primaryColor: '#0033cc',
    secondaryColor: '#050060',
  },

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'darcula',
  },

  stylesheets: [
    {
      rel: "preconnect",
      href: "https://fonts.gstatic.com",
      crossOrigin: "anonymous"
    },
    {
      rel: "preload",
      href: "https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700&display=swap",
      as: "style"
    },
    "https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700&display=swap"
  ],

  fonts: {
    epFont: [
      'Montserrat',
      'Helvetica',
      'Arial',
      'sans-serif'
    ]
  },

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  docsSideNavCollapsible: true,
}
